# frozen_string_literal: true

require 'rails_helper'

describe SetInterviewDateController, type: :controller do
  describe '#create' do
    let(:parameters) do
      {
        interview_event: { interview_date: interview_date },
        id: interview.id,
      }
    end

    let(:interview_date) { '2019-01-01' }
    let(:interview) { create(:interview) }

    let(:last_event) { interview.events.last }

    it 'redirects and creates event' do
      post :create, params: parameters
      expect(response).to have_http_status(:redirect)
      expect(last_event).to be_kind_of(Interview::Event::SetInterviewDate)
      expect(last_event.interview_date).to eq(interview_date)
    end

    context 'empty date' do
      let(:interview_date) { '' }

      it 'shows error and does not emit event' do
        post :create, params: parameters
        expect(response).to have_http_status(:ok)
        expect(interview.events).to be_empty
      end
    end
  end
end
