# frozen_string_literal: true

require 'rails_helper'

describe Interview, type: :model do
  describe 'event attributes' do
    it 'responds to them' do
      described_class::EVENT_ATTRIBUTES.each do |attribute|
        expect(subject).to respond_to(attribute)
      end
    end
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:candidate_name) }
    it { is_expected.to validate_presence_of(:company_name) }
    it { is_expected.to validate_presence_of(:job_name) }
  end

  describe '#emit_event' do
    let(:first_interview_date) { '2019-01-01' }
    let(:second_interview_date) { '2019-01-01' }

    subject { create(:interview) }

    it 'works' do
      expect(subject.events.length).to be_zero
      expect(subject.interview_date).to be_nil

      subject.emit_event(Interview::Event::SetInterviewDate, interview_date: first_interview_date)
      expect(subject.events.length).to eq(1)
      expect(subject.interview_date).to eq(first_interview_date)

      subject.emit_event(Interview::Event::SetInterviewDate, interview_date: second_interview_date)
      expect(subject.events.length).to eq(2)
      expect(subject.interview_date).to eq(second_interview_date)
    end
  end
end
