# frozen_string_literal: true

require 'rails_helper'

describe Interview::Event::SetInterviewDate, type: :model do
  it_behaves_like 'interview event'

  describe '#interview_date' do
    it 'responds' do
      is_expected.to respond_to(:interview_date)
      is_expected.to respond_to(:interview_date=)
    end

    it { is_expected.to validate_presence_of(:interview_date) }

    it do
      is_expected.to allow_values(
        '2019-01-01',
        '2020-03-30',
      ).for(:interview_date)
    end

    it do
      is_expected.to_not allow_values(
        '2019',
        '2020.03.30',
        '30.03.2019',
        '30/03/2019',
        '2020-03-30 12:00',
        nil,
      ).for(:interview_date)
    end
  end
end
