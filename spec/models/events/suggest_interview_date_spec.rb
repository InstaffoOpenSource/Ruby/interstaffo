# frozen_string_literal: true

require 'rails_helper'

describe Interview::Event::SuggestInterviewDate, type: :model do
  it_behaves_like 'interview event'

  describe '#suggested_date' do
    it 'responds' do
      is_expected.to respond_to(:suggested_date)
      is_expected.to respond_to(:suggested_date=)
    end

    it { is_expected.to validate_presence_of(:suggested_date) }

    it do
      is_expected.to allow_values(
        '2019-01-01',
        '2020-03-30',
      ).for(:suggested_date)
    end

    it do
      is_expected.to_not allow_values(
        '2019',
        '2020.03.30',
        '30.03.2019',
        '30/03/2019',
        '2020-03-30 12:00',
        nil,
      ).for(:suggested_date)
    end
  end
end
