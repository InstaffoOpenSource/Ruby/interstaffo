# frozen_string_literal: true

FactoryBot.define do
  factory :interview do
    candidate_name { 'Senior Hodger' }
    company_name { 'Interstaffo GmbH' }
    job_name { 'Crash-Test-Dummy' }
  end
end
