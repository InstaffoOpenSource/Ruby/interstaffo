shared_examples 'interview event' do
  describe '#interview' do
    it { is_expected.to validate_presence_of(:interview) }
  end
end
