Rails.application.routes.draw do
  root 'interviews#index'

  resources :interviews do
    member do
      get :set_date, controller: :set_interview_date, action: :new
      post :set_date, controller: :set_interview_date, action: :create

      get :suggest_date, controller: :suggest_interview_date, action: :new
      post :suggest_date, controller: :suggest_interview_date, action: :create
    end
  end
end
