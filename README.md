# Interstaffo

Interstaffo is a system for managing interviews between companies and candidates.

## Dev Setup

This project has no dependencies on external services and uses SQLite.

- `bin/bundle`
- `bin/rails server`

## Tests

This project uses `rspec` for tests, which can be run using `bin/rspec`.

This project also uses rubocop for linting, which can be run using `bin/lint`.

## Copyright

This is proprietary interview project, which was produced on behalf of Instaffo GmbH. Copyright and licensing ability completely and exclusively lie with Instaffo GmbH.
