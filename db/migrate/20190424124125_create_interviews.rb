class CreateInterviews < ActiveRecord::Migration[5.2]
  def change
    create_table :interviews do |t|
      t.string :candidate_name, null: false
      t.string :company_name, null: false
      t.string :job_name, null: false

      t.timestamps
    end
  end
end
