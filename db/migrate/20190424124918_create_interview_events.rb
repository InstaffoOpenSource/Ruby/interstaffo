class CreateInterviewEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :interview_events do |t|
      t.integer :interview_id
      t.string :type
      t.text :data

      t.timestamps
    end

    add_foreign_key :interview_events, :interviews
  end
end
