class InterviewsController < ApplicationController
  before_action :set_interview, only: %i(show edit update destroy)

  def index
    @interviews = Interview.all
  end

  def show; end

  def new
    @interview = Interview.new
  end

  def edit; end

  def create
    @interview = Interview.new(interview_params)

    if @interview.save
      redirect_to @interview, notice: 'Interview was successfully created.'
    else
      render :new
    end
  end

  def update
    if @interview.update(interview_params)
      redirect_to @interview, notice: 'Interview was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @interview.destroy
    redirect_to interviews_url, notice: 'Interview was successfully destroyed.'
  end

  private

  def set_interview
    @interview = Interview.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def interview_params
    params.require(:interview).permit(:candidate_name, :company_name, :job_name)
  end
end
