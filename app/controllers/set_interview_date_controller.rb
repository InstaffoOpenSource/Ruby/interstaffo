class SetInterviewDateController < ApplicationController
  before_action :interview, only: %i(new create)

  def new
    @interview_event = Interview::Event::SetInterviewDate.new(interview: interview)
  end

  def create
    @interview_event = interview.emit_event(Interview::Event::SetInterviewDate, interview_params)

    if @interview_event.persisted?
      redirect_to interview, notice: 'Interview date was successfully set.'
    else
      render :new
    end
  end

  private

  def interview
    @interview ||= Interview.find(params[:id])
  end

  def interview_params
    params.require(:interview_event).permit(:interview_date)
  end
end
