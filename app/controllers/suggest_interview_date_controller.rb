class SuggestInterviewDateController < ApplicationController
  before_action :interview, only: %i(new create)

  def new
    @interview_event = Interview::Event::SuggestInterviewDate.new(interview: interview)
  end

  def create
    @interview_event = interview.emit_event(Interview::Event::SuggestInterviewDate, interview_params)

    if @interview_event.persisted?
      redirect_to interview, notice: 'Interview date was successfully suggested.'
    else
      render :new
    end
  end

  private

  def interview
    @interview ||= Interview.find(params[:id])
  end

  def interview_params
    params.require(:interview_event).permit(:suggested_date)
  end
end
