class Interview < ApplicationRecord
  include Interview::WithEvents

  validates :candidate_name, presence: true
  validates :job_name, presence: true
  validates :company_name, presence: true

  # strip and presence each string before giving it to validation
  normalize :candidate_name, :job_name, :company_name
end
