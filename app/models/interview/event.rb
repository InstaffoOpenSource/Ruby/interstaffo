class Interview::Event < ApplicationRecord
  belongs_to :interview

  validates :interview, presence: true

  ISO_DATE_REGEX = /\A[0-9]{4}-[0-9]{2}-[0-9]{2}\z/.freeze
end
