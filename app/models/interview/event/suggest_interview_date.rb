class Interview::Event::SuggestInterviewDate < Interview::Event
  # we add a virtual accessor interview_date which
  # is serialized into the event data
  store :data, accessors: %i(suggested_date)

  validates :suggested_date,
            presence: true,
            format: { with: ISO_DATE_REGEX, message: 'not iso format' }
end
