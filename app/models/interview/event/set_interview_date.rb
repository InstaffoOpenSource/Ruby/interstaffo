class Interview::Event::SetInterviewDate < Interview::Event
  # we add a virtual accessor interview_date which
  # is serialized into the event data
  store :data, accessors: %i(interview_date)

  validates :interview_date,
            presence: true,
            format: { with: ISO_DATE_REGEX, message: 'not iso format' }
end
