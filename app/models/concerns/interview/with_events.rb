class Interview < ApplicationRecord
  module WithEvents
    extend ActiveSupport::Concern

    included do
      has_many :events, class_name: 'Interview::Event'

      EVENT_ATTRIBUTES = %i(interview_date suggested_date).freeze

      # add event attributes as methods on interview
      EVENT_ATTRIBUTES.each do |attribute|
        define_method attribute do
          # find last event that defined this attribute
          events.reverse.find do |event|
            event.respond_to?(attribute)
          end.try!(attribute)
        end
      end
    end

    def emit_event(klass, arguments = {})
      klass.new(arguments.merge(interview: self)).tap do |event|
        # if save was successful, reload so that event relation is updated
        reload if event.save
      end
    end
  end
end
